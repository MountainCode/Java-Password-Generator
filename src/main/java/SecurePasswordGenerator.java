import java.security.SecureRandom;

import org.apache.commons.lang.RandomStringUtils;

public class SecurePasswordGenerator {
  public static void main(String[] args) {
    System.out.println(genRandom());
  }
  public static String genRandom() {
    char[] chars = new char[95];
    for(int i = 32; i < 127; ++i) {
      chars[i - 32] = (char) i;
    }
    return RandomStringUtils.random(32, 0, 95, false, false, chars, new SecureRandom());
  }
}
