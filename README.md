# SecurePasswordGenerator

Generates secure random passwords.  Can be useful for generating other secure random strings.

### Usage

You need JRuby with the buildr gem installed.

```$ buildr run```

Change the first argument to RandomStringUtils.random() if you want a different length string.
