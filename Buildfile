repositories.remote << 'http://repo1.maven.org/maven2/'

define 'password-generator' do
  project.version = '0.1.0'
  compile.with 'commons-lang:commons-lang:jar:2.3'
  run.using :main => 'SecurePasswordGenerator'
end
